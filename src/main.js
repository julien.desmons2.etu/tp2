import Component from './Component.js';
import data from './data.js';
import PizzaList from './pages/PizzaList.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();
const pizza = data[0];
const pizzaList = new PizzaList(data);
document.querySelector('.pageContent').innerHTML = pizzaList.render();
