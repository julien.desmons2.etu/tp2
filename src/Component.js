import Img from './Img.js';

class Component {
	tagname;
	children;
	attribute;

	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (!this.children) {
			if (this.attribute) {
				return `<${this.tagName} ${this.renderAttribute()} />`;
			}
			return `<${this.tagName} />`;
		} else {
			if (this.attribute) {
				return `<${
					this.tagName
				} ${this.renderAttribute()} >${this.renderChildren()}</${
					this.tagName
				}>`;
			}
			return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
		}
	}

	renderAttribute() {
		return `${this.attribute.name}=${this.attribute.value}`;
	}

	renderChildren() {
		if (this.children instanceof Array) {
			var s = '';
			this.children.forEach(element => {
				if (element instanceof Component || element instanceof Img) {
					s = s + element.render();
				} else {
					s = s + element;
				}
			});
			return s;
		}
		return this.children;
	}
}

export default Component;
