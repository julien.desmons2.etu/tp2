import Component from './../Component.js';
import PizzaThumbnail from './../PizzaThumbnail.js';
class PizzaList extends Component {
	data;

	constructor(data) {
		super(
			'section',
			{ name: 'class', value: 'pizzaLis' },
			PizzaList.getList(data)
		);
	}

	static getList(data) {
		const res = [];
		data.forEach(element => {
			res.push(new PizzaThumbnail(element));
		});
		return res;
	}
}

export default PizzaList;
