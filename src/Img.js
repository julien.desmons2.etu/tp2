class Img {
	link;

	constructor(link) {
		this.link = link;
	}

	render() {
		return `<img src=${this.link} />`;
	}
}
export default Img;
